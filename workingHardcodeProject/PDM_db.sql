CREATE TABLE Title(
	titleID INT NOT NULL IDENTITY(1,1),
	title VARCHAR(500),

	CONSTRAINT title_pk PRIMARY KEY(titleID)
);

CREATE TABLE Question(
	qID INT NOT NULL IDENTITY(1,1),
	tID INT NOT NULL,
	question VARCHAR(max) NOT NULL,
	poster VARCHAR(500) NOT NULL,
	postdate DATE NOT NULL,
	
	CONSTRAINT question_pk PRIMARY KEY (qID),
	CONSTRAINT title_fk FOREIGN KEY (tID) REFERENCES Title(titleID)
	
	ON UPDATE CASCADE
	ON DELETE CASCADE
);

CREATE TABLE Answer(
	answerID INT NOT NULL IDENTITY(1,1),
	qID INT NOT NULL,
	answer VARCHAR(max) NOT NULL,
	poster VARCHAR(500) NOT NULL,
	answerdate DATE NOT NULL,
	
	CONSTRAINT answer_pk PRIMARY KEY (answerID),
	CONSTRAINT question_fk FOREIGN KEY (qID) REFERENCES Question(qID)
	
	ON UPDATE CASCADE
	ON DELETE CASCADE
);

CREATE TABLE Users(
	username VARCHAR(50) NOT NULL,
	email VARCHAR(100) UNIQUE NOT NULL,
	pwd VARCHAR(50) UNIQUE NOT NULL,
	usertype CHAR(5) NOT NULL,
	
	CONSTRAINT user_pk PRIMARY KEY (username),
	CONSTRAINT user_chk CHECK (userType='User' OR usertype='Admin')
);

ALTER TABLE Question
ADD category VARCHAR(50);

select * from Title
select * from Users
select * from Question
select * from Answer