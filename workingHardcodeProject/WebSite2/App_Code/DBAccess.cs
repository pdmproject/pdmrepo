﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DBAccess
/// </summary>
public class DBAccess
{
    SqlConnection conn;
    public DBAccess()
    {
        conn = ConnectionManager.GetConnection();
    }

    public void addPost(String title)
    {
        if (conn.State.ToString() == "Closed")
        {
            conn.Open();
        }
        String sql = "insert into Title (title) values ('" + title + "')";
        SqlCommand cmd = new SqlCommand(sql, conn);
        cmd.ExecuteNonQuery();
        conn.Close();
    }

    public void addUser(String username, String email, String password, String type)
    {
        if (conn.State.ToString() == "Closed")
        {
            conn.Open();
        }
        String sql = "insert into Users (username, email, pwd, usertype) values ('" + username + "', '" + email + "','" + password + "', '" + type + "')";
        SqlCommand cmd = new SqlCommand(sql, conn);
        cmd.ExecuteNonQuery();
        conn.Close();
    }

    public bool addPostContent(string title, string post, string category, string poster, string postdate)
    {
        bool status = false;
        if (conn.State.ToString() == "Closed")
        {
            conn.Open();
        }
        SqlCommand cmd = conn.CreateCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into dbo.Question (title, question, category, poster, postdate) values('" + title + "','" + post + "','" + category + "','" + poster + "','" + postdate + "')";
        cmd.ExecuteNonQuery();
        status = true;
        return status;
    }

    public DataSet getAllPosts()
    {
        if (conn.State.ToString() == "Closed")
        {
            conn.Open();
        }

        SqlCommand cmd = conn.CreateCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select title from dbo.Question";

        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds, "Question");
        conn.Close();
        return ds;
    }
}

