﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Register : System.Web.UI.Page
{
    DBAccess db = new DBAccess();

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSignup_Click(object sender, EventArgs e)
    {
        String email = TextBox4.Text;
        String name = TextBox5.Text;
        String passw = TextBox6.Text;
        String type = RadioButtonList1.SelectedValue;
        db.addUser(name,email,passw,type);
        TextBox4.Text = "";
        TextBox5.Text = "";

        string msg = "You have succefully sign up for CurtinSquare";
        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + msg + "');", true);
        Response.Redirect("Login.aspx");

    }
}