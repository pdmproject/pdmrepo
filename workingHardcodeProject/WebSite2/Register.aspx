﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
    .style6
    {
            width: 261px;
        }
    .style7
    {
        height: 63px;
    }
    .style8
    {
        width: 122px;
        height: 63px;
    }
    .style9
    {
        height: 26px;
    }
    .style10
    {
        width: 122px;
        height: 26px;
    }
    .style11
    {
        width: 122px;
    }
    .style12
    {
        height: 25px;
    }
    .style13
    {
        width: 122px;
        height: 25px;
    }
    .style14
    {
        height: 28px;
    }
    .style15
    {
        width: 122px;
        height: 28px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div style="height: 183px">
    <table style="width:100%;">
        <tr>
            <td class="style14">
                &nbsp;</td>
            <td class="style15">
                &nbsp;</td>
            <td class="style14">
                .edu</td>
        </tr>
        <tr>
            <td class="style12">
                Email:</td>
            <td class="style13">
                <asp:TextBox ID="TextBox4" runat="server" Width="340px"></asp:TextBox>
            </td>
            <td class="style12">
                @curtin.au.edu</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblName" runat="server" Text="Full Name:"></asp:Label>
            </td>
            <td class="style11">
                <asp:TextBox ID="TextBox5" runat="server" Width="345px" Height="21px"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style9">
                <asp:Label ID="lblPwd" runat="server" Text="Password"></asp:Label>
            </td>
            <td class="style10">
                <asp:TextBox ID="TextBox6" runat="server" Width="344px" Height="22px" 
                    TextMode="Password"></asp:TextBox>
            </td>
            <td class="style9">
            </td>
        </tr>
        <tr>
            <td class="style9">
                <asp:Label ID="lblConfPwd" runat="server" Text="Retype-Password"></asp:Label>
            </td>
            <td class="style10">
                <asp:TextBox ID="TextBox7" runat="server" Width="344px" Height="20px" 
                    TextMode="Password"></asp:TextBox>
            </td>
            <td class="style9">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style7">
            </td>
            <td class="style8">
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" Height="16px" 
                    RepeatDirection="Horizontal" Width="352px">
                    <asp:ListItem Value="User">Student</asp:ListItem>
                    <asp:ListItem Value="User">Academic Staff</asp:ListItem>
                    <asp:ListItem Value="User">Non-Academic Staff</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td class="style7">
            </td>
        </tr>
        <tr>
            <td class="style7">
                &nbsp;</td>
            <td class="style8">
                <asp:Button ID="btnSignup" runat="server" style="margin-left: 125px" 
                    Text="Sign Up" onclick="btnSignup_Click" />
            </td>
            <td class="style7">
                &nbsp;</td>
        </tr>
    </table>
</div>
<p>
</p>
<br />
</asp:Content>

