﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class challenge : System.Web.UI.Page
{
    DBAccess dba = new DBAccess();

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_Chalng_Click(object sender, EventArgs e)
    {
        string post = txtPost.Text;
        string title = txtTitle.Text;
        string posdate = DateTime.Now.ToString();

        if (post == "" || title == "")
        {
            lblPost.Text = "Please make sure you have filled all fields.";
        }
        else
        {
            if (dba.addPostContent(title, post, "Computing", "Spencer Hastings", posdate))
            {
                lblPost.Text = "Your post was added successfully.";
                txtPost.Enabled = false;
                txtTitle.Enabled = false;
            }
            else
            {
                lblPost.Text = "Oops! Something went wrong. Try again later.";
            }
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}