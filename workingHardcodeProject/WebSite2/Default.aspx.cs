﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class _Default : System.Web.UI.Page
{
    DBAccess dba = new DBAccess();

    protected void Page_Load(object sender, EventArgs e)
    {
        FillPostGrid();
    }
   /* protected void Button1_Click(object sender, EventArgs e)
    {
        
    }*/
    protected void btnReply_Click(object sender, EventArgs e)
    {
        Response.Redirect("Discussion.aspx");
    }

    public void FillPostGrid()
    {
        DataSet ds = dba.getAllPosts();
        dgvPosts.DataSource = ds.Tables["Question"].DefaultView;
    }
}
