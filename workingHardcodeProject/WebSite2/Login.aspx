﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style8
        {
            width: 262px;
        }
        .style9
        {
            width: 222px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>
        <asp:Label ID="Label1" runat="server" Font-Size="Medium" ForeColor="#0033CC" 
            Text="User Login"></asp:Label>
        <br />
        <br />
        <table style="width:100%;">
            <tr>
                <td class="style8" align="right">
                    Username</td>
                <td class="style9">
                    &nbsp;
                    <asp:TextBox ID="txtUsername" runat="server" Width="206px"></asp:TextBox>
                    &nbsp;</td>
                <td>
                    <asp:RequiredFieldValidator ID="userValidation" runat="server" 
                        ErrorMessage="*Please enter username" ValidationGroup="login" Visible="False"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style8" align="right">
                    Password</td>
                <td class="style9">
                    &nbsp;
                    <asp:TextBox ID="txtPassword" runat="server" Width="207px" TextMode="Password"></asp:TextBox>
                    &nbsp;</td>
                <td>
                    <asp:RequiredFieldValidator ID="pwdValidation" runat="server" 
                        ErrorMessage="*Please enter password" ValidationGroup="login" Visible="False"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style8">
                    &nbsp;</td>
                <td class="style9" align="center">
                    <asp:Button ID="btnLogin" runat="server" Text="Login" ValidationGroup="login" 
                        onclick="btnLogin_Click" />
                &nbsp;
                    <asp:Button ID="btnClear" runat="server" Text="Clear" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <br />
        <br />
        <br />
    </p>
</asp:Content>

